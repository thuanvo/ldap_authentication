Gem::Specification.new do |s|
  s.name        = 'ldap_auth'
  s.version     = '0.0.1'
  s.date        = '2015-04-16'
  s.summary     = "allow authenticate with LDAP"
  s.description = "allow authenticate with LDAP"
  s.authors     = ["mr Log"]
  s.email       = 'vanthuankhtn@gmail.com'
  s.files       = ["lib/ldap_auth.rb"]
  s.homepage    = 'http://rubygems.org/gems/ldap_auth'
  s.license       = 'MIT'

  s.add_dependency "net-ldap"
end