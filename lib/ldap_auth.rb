require "yaml"
require "net-ldap"
require_relative "ldap_user"

class LdapAuth < Rails::Application
  cattr_accessor :base, :ldap
  
  def self.initialize_ldap_con
    config = YAML.load(ERB.new(File.read("#{Rails.root}/config/ldap.yml")).result)[Rails.env]
    options = {:host => config['host'],
               :port => config['port'],
               :base => config['base'],
               :encryption => config['ssl'],
               :auth => {
                   :method => :simple,
                   :username => config['admin_user'],
                   :password => config['admin_password']
              }
    }

    self.base = config['base']
    self.ldap = Net::LDAP.new options
  end

  #authenticate with LDAP
  def self.authenticate(username, password)
    self.initialize_ldap_con if self.ldap.nil?
      
    result = self.ldap.bind_as(
        :base => self.base,
        :filter => "(uid=#{username})",
        :password => password
    )
    self.get_user(result)
  end

  #get user after authenticated
  def self.get_user(result)
    if result
      first_item = result.first      
      user = LdapUser.new
      user.uid = first_item.uid
      user.name = first_item.givenName
      user.email = first_item.mail    
      return user
    end
    return nil
  end

end
